package com.aesha.a20220428_aeshalad_nycschools

import com.aesha.a20220428_aeshalad_nycschools.schoolinfo.datamodel.DetailDataModel
import com.aesha.a20220428_aeshalad_nycschools.schoolinfo.datamodel.SchoolHistoryModel
import com.aesha.a20220428_aeshalad_nycschools.schoolinfo.repository.ApiClient
import com.aesha.a20220428_aeshalad_nycschools.schoolinfo.repository.Repository
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class MainRepositoryTest {
    private lateinit var repository: Repository

    @Mock
    lateinit var apiClient: ApiClient

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        apiClient = ApiClient.create()
        repository=Repository(apiClient)
    }

    @Test
    fun `get school history test`() {
        runBlocking {
            Mockito.`when`(apiClient.getSchoolDetails())
                .thenReturn(Response.success(listOf()))
            val response = repository.getSchoolDetails()
            assertEquals(listOf<SchoolHistoryModel>(), response.body())
        }
    }

    @Test
    fun `get SAT history test`() {
        runBlocking {
            Mockito.`when`(apiClient.getSATDetails())
                .thenReturn(Response.success(listOf()))
            val response=repository.getSATDetails()
            assertEquals(listOf<DetailDataModel>(), response.body())
        }
    }
}